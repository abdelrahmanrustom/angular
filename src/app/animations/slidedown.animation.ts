import { trigger, style, state, transition, animate, keyframes } from '@angular/animations';

export const slideDown = trigger('slideDown', [

  transition('void => *', [
    animate(1000, keyframes([
      style({
        opacity: 0,
        transform: 'translateY(-100%)',
        offset: 0
      }),
      style({
        opacity: 0.5,
        transform: 'translateY(-50%)',
        offset: 0.3
      }),
      style({
        opacity: 0.9,
        offset: 0.8,
        transform: 'translateY(-20%)'
      }),
      style({
        opacity: 0.9,
        transform: 'translateY(0)',
        offset: 1
      })
    ]))
  ]),
]);
