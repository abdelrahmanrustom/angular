import { trigger, style, state, transition, animate, keyframes } from '@angular/animations';

export const slideIn = trigger('slideIn', [
  transition('void => *', [
    animate(1000, keyframes([
      style({
        opacity: 0,
        transform: 'translateX(-100%)',
        offset: 0
      }),
      style({
        opacity: 0.5,
        transform: 'translateX(-50%)',
        offset: 0.3
      }),
      style({
        opacity: 0.9,
        offset: 0.8,
        transform: 'translateX(-20%)'
      }),
      style({
        opacity: 0.9,
        transform: 'translateX(0)',
        offset: 1
      })
    ]))
  ]),
]);