import { Component, ChangeDetectorRef, OnDestroy, OnInit, AfterContentChecked } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Store, Select } from '@ngxs/store';
import { UserLoginAction } from '../../actions/user.action';
import { UserState, UserStateModel } from '../../state/user.state';
import { LocalStorageService } from '../../utils/local.storage';
import { environment } from '../../../environments/environment';
import { slideDown } from 'src/app/animations/slidedown.animation';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [slideDown]
})
export class LoginComponent implements OnInit, OnDestroy, AfterContentChecked {
  siginInForm: FormGroup;
  @Select(UserState) userState$: Observable<UserStateModel>;
  storeSub: Subscription;
  state: UserStateModel;

  constructor(private formBuilder: FormBuilder,
    private store: Store,
    private chr: ChangeDetectorRef,
    private router: Router,
    private localStorage: LocalStorageService
  ) { }

  ngOnInit() {
    this.siginInForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
    this.storeSub = this.userState$.subscribe((state: UserStateModel) => {
      this.state = { ...state };
      this.chr.detectChanges();
    });
    if (this.localStorage.loadFromLocalStorage(environment.getUser) !== null) {
      this.router.navigate(['home']);
    }
  }

  signIn(email, password) {
    this.store.dispatch(new UserLoginAction({ email, password }));
  }

  ngOnDestroy() {
    this.storeSub.unsubscribe();
  }

  // this three methods is called after state is being changed ngDoCheck then ngAfterContentChecked then ngAfterViewChecked
  // ngAfterViewChecked() {}

  ngAfterContentChecked() {
    if (this.state.user && this.state.user.user_data && this.localStorage.loadFromLocalStorage(environment.getUser) !== null) {
      this.router.navigate(['/home']);
    }
  }
  // ngDoCheck() {}
}
