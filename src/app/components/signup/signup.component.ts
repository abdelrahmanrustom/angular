import { Component, OnInit, OnDestroy, AfterContentChecked } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Store, Select } from '@ngxs/store';
import { UserRegisterationAction, AreasAction, EditUserAction } from '../../actions/user.action';
import { CategoriesAction } from '../../actions/business.action';
import { UserState, UserStateModel } from '../../state/user.state';
import { BusinessState, BusinessStateModel } from '../../state/business.state';
import { LocalStorageService } from '../../utils/local.storage';
import { environment } from '../../../environments/environment';
import { User } from '../../models/User';
import { slideIn } from 'src/app/animations/slidein.animation';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  animations: [slideIn]
})
export class SignupComponent implements OnInit, OnDestroy, AfterContentChecked {

  registerForm: FormGroup;
  selectedPath = './assets/take_photo.png';
  genderList: object[] = [
    { id: 'male', value: 'Male' },
    { id: 'female', value: 'Female' }
  ];
  countryList: object[] = [
    { id: '5', value: 'United States' },
    { id: '424', value: 'United Arab Emirates' },
    { id: '602', value: 'Egypt' }
  ];
  interestsSelect: any[] = [];
  gender: string;
  country: string;
  area: number;
  interests = new FormControl();
  areas = new FormControl();
  @Select(UserState) userState$: Observable<UserStateModel>;
  @Select(BusinessState) businessState$: Observable<BusinessStateModel>;
  userSub: Subscription;
  businessSub: Subscription;
  userState: UserStateModel;
  businessState: BusinessStateModel;

  constructor(private formBuilder: FormBuilder,
    private store: Store,
    private router: Router,
    private localStorage: LocalStorageService
  ) { }

  ngOnInit() {
    this.store.dispatch(new CategoriesAction(null));

    if (this.localStorage.loadFromLocalStorage(environment.getUser) as User) {
      this.router.navigate(['home']);
    }

    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      mobilenumber: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(11)]],
      area: ['', [Validators.required]]
      // recaptcha: ['', Validators.required]
    });
    this.userSub = this.userState$.subscribe((state: UserStateModel) => {
      this.userState = { ...state };
    });
    this.businessSub = this.businessState$.subscribe((state: BusinessStateModel) => {
      this.businessState = { ...state };
    });
    this.store.dispatch(new AreasAction({ city_id: 1 }));
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
    this.businessSub.unsubscribe();
  }

  onFileChanged(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      const reader: any = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      console.log(event.target.files[0]);
      // tslint:disable-next-line:no-shadowed-variable
      reader.onload = (event) => { // called once readAsDataURL is completed
        if (event.target.result !== null) {
          this.selectedPath = event.target.result;
          console.log(event);
        }
      };
    }
  }

  signup(name, email, password, mobilenumber, birthdate) {
    let birthdateRightFormatForBackend;
    if (birthdate) {
      const splitted = birthdate.split('/', 3);
      birthdateRightFormatForBackend = splitted[2] + '-' + splitted[0] + '-' + splitted[1];
    }
    this.store.dispatch(new UserRegisterationAction({
      name, email, password, mobile: mobilenumber,
      birthdate: birthdateRightFormatForBackend, gender: this.gender,
      media: this.selectedPath
    }));
  }

  selectGender(gender) {
    this.gender = gender;
  }

  selectCountry(country) {
    this.country = country;
  }

  selectInterests(event) {
    this.interestsSelect = event.source.selected.map(item => item.value);
  }

  selectArea(area) {
    this.area = area.value;
  }

  ngAfterContentChecked() {
    if (this.userState.user && this.userState.user.user_data) {
      if (!this.userState.editUser) {
        let categories;
        if (this.interestsSelect || this.area) {
          if (this.interestsSelect) {
            categories = this.interestsSelect[0];
            for (let i = this.interestsSelect.length - 1; i > 0; i--) {
              categories = categories + ',' + this.interestsSelect[i];
            }
          }
          this.store.dispatch(new EditUserAction({ area: this.area, category_ids: categories }));
        }
      } else {
        this.router.navigate(['/home']);
      }
    }
  }

}
