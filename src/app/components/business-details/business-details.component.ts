import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { Business } from 'src/app/models/Business';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from '../../utils/local.storage';
import { environment } from '../../../environments/environment';
import { MatTabChangeEvent } from '@angular/material';
import { Store, Select } from '@ngxs/store';
import { BusinessStateModel, BusinessState } from 'src/app/state/business.state';
import { Subscription, Observable } from 'rxjs';
import { GetReviewsAction, CreateReviewAction } from 'src/app/actions/business.action';
import { Review } from 'src/app/models/Review';
import { slideDown } from 'src/app/animations/slidedown.animation';

@Component({
  selector: 'app-business-details',
  templateUrl: './business-details.component.html',
  styleUrls: ['./business-details.component.css'],
  animations: [slideDown]
})
export class BusinessDetailsComponent implements OnInit, AfterContentChecked {

  business: Business;
  reviews: Review[];
  @Select(BusinessState) businessState$: Observable<BusinessStateModel>;
  businessSub: Subscription;
  businessState: BusinessStateModel;

  constructor(private localStorage: LocalStorageService,
    private store: Store) { }

  ngOnInit() {
    this.businessSub = this.businessState$.subscribe((state: BusinessStateModel) => {
      this.businessState = { ...state };
    });

    this.business = (this.localStorage.loadFromLocalStorage(environment.saveBusiness) as Business);
  }

  openLink(lng, lat) {
    window.open('https://www.google.com/maps/search/?api=1&query=' + lat + ',' + lng, '_blank');
  }

  tabClicked(tab: MatTabChangeEvent) {
    if (tab.tab.textLabel === 'Reviews') {
      this.getReviews();
    }
  }

  getReviews() {
    this.store.dispatch(new GetReviewsAction({ branch_id: this.business.branch.id }));
  }

  ngAfterContentChecked() {
    if (this.businessState.reviews) {
      this.reviews = this.businessState.reviews.reviews;
      // this.total = this.businessState.businesses.pagination.total_pages_no*10;
    }
  }

  addComment(comment) {
    this.store.dispatch(new CreateReviewAction({ text: comment, branch_id: this.business.branch.id }));
  }

}
