import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/utils/local.storage';
import { PreventLoggedInAccess } from 'src/app/utils/login.access';
import { User } from 'src/app/models/User';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterContentChecked {
  navigateToDashboard: boolean;
  constructor(private router: Router, private localStorage: LocalStorageService, public loggedInAccess: PreventLoggedInAccess) {

  }

  ngOnInit() { }

  ngAfterContentChecked() {
    const user = this.localStorage.loadFromLocalStorage(environment.getUser) as User;
    if (user) {
      user.user_data.type === 'user' ? this.navigateToDashboard = false : this.navigateToDashboard = true;
    }
  }

  login() {
    this.router.navigate(['']);
  }


  signup() {
    this.router.navigate(['/signup']);
  }

  logout() {
    this.localStorage.removeFromLocalStorage(environment.getUser);
    this.navigateToDashboard = false;
    this.loggedInAccess.loggedInFlag();
    this.router.navigate(['']);
  }

  home() {
    this.router.navigate(['']);
  }

  profile() {
    this.router.navigate(['/profile']);
  }

  dashboard() {
    const user = this.localStorage.loadFromLocalStorage(environment.getUser) as User;
    if (this.navigateToDashboard) {
      this.router.navigate(['/dashboard']);
    }

  }
}
