import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CategoriesAction } from '../../actions/business.action';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { BusinessState, BusinessStateModel } from '../../state/business.state';
import { slideDown } from 'src/app/animations/slidedown.animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [slideDown]
})
export class HomeComponent implements OnInit {

  searchForm: FormGroup;
  category = -1;
  categories = new FormControl();
  @Select(BusinessState) businessState$: Observable<BusinessStateModel>;
  businessSub: Subscription;
  businessState: BusinessStateModel;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new CategoriesAction(null));
    this.searchForm = this.formBuilder.group({
      search: ['', [Validators.required]],
    });
    this.businessSub = this.businessState$.subscribe((state: BusinessStateModel) => {
      this.businessState = { ...state };
    });
  }

  selectCategory(category) {
    this.category = category.value;
  }

  searchBusiness(search) {
    this.router.navigate(['/businesses', { type: 'search', value: search }]);
  }

  searchBusinessByCat() {
    if (this.category !== -1) {
      this.router.navigate(['/businesses', { type: 'category', value: this.category }]);
    }
  }
}
