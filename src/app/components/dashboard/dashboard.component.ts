import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../../canvasjs-2.2/canvasjs.min';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const chart = new CanvasJS.Chart('chartContainer', {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: 'Number of businesses according to category'
      },
      data: [{
        type: 'column',
        dataPoints: [
          { y: 12178, label: 'Food' },
          { y: 594, label: 'Entertainment' },
          { y: 2867, label: 'Real Estate' },
          { y: 554, label: 'Kiosk' },
          { y: 361, label: 'Education' },
          { y: 66, label: 'NGOs' },
          { y: 163, label: 'Photography' },
          { y: 567, label: 'Fashion & Beauty' },
          { y: 477, label: 'Shopping' },
          { y: 31, label: 'Home' },
          { y: 1713, label: 'Sports' },

        ]
      }]
    });

    chart.render();
  }

}
