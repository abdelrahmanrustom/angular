import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Store, Select } from '@ngxs/store';
import { AreasAction, EditUserAction } from '../../actions/user.action';
import { CategoriesAction } from '../../actions/business.action';
import { UserState, UserStateModel } from '../../state/user.state';
import { BusinessState, BusinessStateModel } from '../../state/business.state';
import { LocalStorageService } from '../../utils/local.storage';
import { environment } from '../../../environments/environment';
import { User } from '../../models/User';
import { slideIn } from 'src/app/animations/slidein.animation';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css'],
	animations: [slideIn]
})
export class ProfileComponent implements OnInit, OnDestroy {

	registerForm: FormGroup;
	selectedPath = './assets/take_photo.png';
	selectedFile: File;
	genderList: object[] = [
		{ id: 'male', value: 'Male' },
		{ id: 'female', value: 'Female' }
	];
	interestsSelect: any[] = [];
	gender: string;
	country: string;
	area: number;
	interests = new FormControl();
	areas = new FormControl();
	@Select(UserState) userState$: Observable<UserStateModel>;
	@Select(BusinessState) businessState$: Observable<BusinessStateModel>;
	userSub: Subscription;
	businessSub: Subscription;
	userState: UserStateModel;
	businessState: BusinessStateModel;
	user: User;
	birthdate: String;
	categoriesLoaded = false;

	constructor(private formBuilder: FormBuilder,
		private store: Store,
		private router: Router,
		private localStorage: LocalStorageService) { }



	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			email: ['', [Validators.required, Validators.email]],
			name: ['', [Validators.required]],
			mobilenumber: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(11)]],
			recaptcha: ['', Validators.required],
			birthdate: [''],
			gender: [''],
			area: [''],
			interests: ['']
		});
		this.user = (this.localStorage.loadFromLocalStorage(environment.getUser) as User);
		this.registerForm.controls['name'].setValue(this.user.user_data.name);
		this.registerForm.controls['email'].setValue(this.user.user_data.email);
		this.registerForm.controls['mobilenumber'].setValue(this.user.user_data.mobile);
		this.registerForm.controls['birthdate'].setValue(this.user.user_data.birthdate);
		this.registerForm.controls['gender'].setValue(this.user.user_data.gender);
		this.registerForm.controls['area'].setValue(this.user.user_data.area.id);
		this.selectedPath = this.user.user_data.profile_photo;
		this.userSub = this.userState$.subscribe((state: UserStateModel) => {
			this.userState = { ...state };
		});
		this.businessSub = this.businessState$.subscribe((state: BusinessStateModel) => {
			this.businessState = { ...state };
		});
		this.store.dispatch(new CategoriesAction(null));
		this.store.dispatch(new AreasAction({ city_id: 1 }));
	}

	ngOnDestroy() {
		this.userSub.unsubscribe();
		this.businessSub.unsubscribe();
	}

	ngAfterContentChecked() {
		if (this.businessState.categories && !this.categoriesLoaded) {
			const userCategories: string[] = this.user.user_data.categories;
			const userCategoriesIds: number[] = [];
			let index: number;
			for (let i = this.businessState.categories.length - 1; i >= 0; i--) {
				index = userCategories.indexOf(this.businessState.categories[i].name);
				if (index !== -1) {
					userCategoriesIds.push(this.businessState.categories[i].id);
				}
			}
			// tslint:disable-next-line:no-unused-expression
			userCategoriesIds.length > 0 ? this.categoriesLoaded = true : null;
			this.registerForm.controls['interests'].setValue(userCategoriesIds);
		}

		if (this.userState.user && this.userState.user.user_data && this.userState.editUser) {
			this.router.navigate(['/home']);
		}


	}

	onFileChanged(imageInput) { // called each time file input changes
		const file: File = imageInput.files[0];
		const reader = new FileReader();
		reader.addEventListener('load', (event: any) => {
			// console.log(file);
			// console.log(event.target.result);
			this.selectedPath = event.target.result;
			this.selectedFile = file;
		});
		reader.readAsDataURL(file);
	}

	editProfile(name, email, mobilenumber, birthdate) {
		let birthdateRightFormatForBackend;
		if (birthdate) {
			const splitted = birthdate.split('/', 3);
			birthdateRightFormatForBackend = splitted[2] + '-' + splitted[0] + '-' + splitted[1];
		}

		var categories: string;
		if (this.interestsSelect) {
			categories = this.interestsSelect[0];
			for (let i = this.interestsSelect.length - 1; i > 0; i--) {
				categories = categories + ',' + this.interestsSelect[i];
			}
		}
		this.store.dispatch(new EditUserAction({
			name, email, mobile: mobilenumber,
			birthdate: birthdateRightFormatForBackend, gender: this.gender, area: this.area,
			media: this.selectedFile, category_ids: categories
		}));

	}

	selectGender(gender) {
		this.gender = gender;
	}

	selectCountry(country) {
		this.country = country;
	}

	selectInterests(event) {
		this.interestsSelect = event.source.selected.map(item => item.value);
	}

	selectArea(area) {
		this.area = area.value;
	}


}
