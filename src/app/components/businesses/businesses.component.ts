import { Component, OnInit, AfterContentChecked, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Business } from 'src/app/models/Business';
import { SearchBusinessesAction } from '../../actions/business.action';
import { BusinessState, BusinessStateModel } from '../../state/business.state';
import { LocalStorageService } from '../../utils/local.storage';
import { environment } from '../../../environments/environment';
import { slideDown } from 'src/app/animations/slidedown.animation';
import { slideIn } from 'src/app/animations/slidein.animation';

@Component({
  selector: 'app-businesses',
  templateUrl: './businesses.component.html',
  styleUrls: ['./businesses.component.css'],
  animations: [slideDown, slideIn]
})
export class BusinessesComponent implements OnInit, AfterContentChecked, OnDestroy {

  @Select(BusinessState) private businessState$: Observable<BusinessStateModel>;
  private businessSub: Subscription;
  businessState: BusinessStateModel;
  private businesses: Business[];
  private page = 1;
  private total = -1;
  private breakpoint = 1;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private store: Store,
    private localStorage: LocalStorageService) { }

  ngOnInit() {
    this.breakpoint = (window.innerWidth <= 414) ? 1 : (window.innerWidth <= 768) ? 3 : 4;
    this.businessSub = this.businessState$.subscribe((state: BusinessStateModel) => {
      this.businessState = { ...state };
    });
    if (this.route.snapshot.paramMap.get('type') === 'search') {
      this.store.dispatch(new SearchBusinessesAction({ name: this.route.snapshot.paramMap.get('value'), page: this.page }));
    } else {
      this.store.dispatch(new SearchBusinessesAction({ category_id: this.route.snapshot.paramMap.get('value'), page: this.page }));
    }
  }

  pageChanged(event) {
    this.page = event;
    if (this.route.snapshot.paramMap.get('type') === 'search') {
      this.store.dispatch(new SearchBusinessesAction({ name: this.route.snapshot.paramMap.get('value'), page: this.page }));
    } else {
      this.store.dispatch(new SearchBusinessesAction({ category_id: this.route.snapshot.paramMap.get('value'), page: this.page }));
    }
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 414) ? 1 : (window.innerWidth <= 768) ? 3 : 4;
  }

  ngAfterContentChecked() {
    console.log(this.businessState.error);
    if (this.businessState.businesses) {
      this.businesses = this.businessState.businesses.businesses;
      this.total = this.businessState.businesses.pagination.total_pages_no * 10;
    }
  }

  openBusiness(business) {
    this.localStorage.storeOnLocalStorage(environment.saveBusiness, business);
    this.router.navigate(['/details', { id: business.id }]);
  }

  ngOnDestroy() {
    this.businessSub.unsubscribe();
  }

}
