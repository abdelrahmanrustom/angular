import { State, Action, StateContext, Selector } from '@ngxs/store';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../models/User';
import { Area } from '../models/Area';
import { Areas } from '../models/Areas';
import { UserLoginAction, UserRegisterationAction, EditUserAction, AreasAction } from '../actions/user.action';
import { LocalStorageService } from '@local-storage';
import { environment } from '../../environments/environment';

export class UserStateModel {
    user: User;
    areas: Area[];
    loading: boolean;
    editUser: boolean;
}

@State<UserStateModel>({
    name: 'user',
    defaults: {
        user: null,
        areas: [],
        loading: false,
        editUser: false
    }
})

export class UserState {
    constructor(private http: HttpClient, private localStorage: LocalStorageService) { }

    @Action(UserLoginAction)
    login({ getState, setState }: StateContext<UserStateModel>, payload: any) {
        const state = getState();
        const body = new HttpParams()
            .set('email', payload.payload.email)
            .set('password', payload.payload.password)
            .set('device_IMEI', '121332434');
        setState({ ...state, loading: true });
        this.http.post<User>(environment.baseUrl + environment.signIn, body).subscribe(user => {
            if (user.user_data) {
                this.localStorage.storeOnLocalStorage(environment.getUser, user);
            }
            setState({
                ...state,
                user: user,
                loading: false
            });
        });
    }

    @Action(UserRegisterationAction)
    register({ getState, setState }: StateContext<UserStateModel>, payload: any) {
        const state = getState();
        const input: any = new FormData();
        // tslint:disable-next-line:no-unused-expression
        payload.payload.name ? input.append('name', payload.payload.name) : null;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.email ? input.append('email', payload.payload.email) : null;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.password ? input.append('password', payload.payload.password) : null;
        input.append('device_IMEI', '121332434');
        // tslint:disable-next-line:no-unused-expression
        payload.payload.gender ? input.append('gender', payload.payload.gender) : null;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.birthdate ? input.append('birthdate', payload.payload.birthdate) : null;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.mobile ? input.append('mobile', payload.payload.mobile) : null;
        // tslint:disable-next-line:quotemark
        input.append("Media[file]'; filename = 'photo.jpg'", payload.payload.media);
        const config: any = new HttpHeaders().set('Content-Type', 'multipart/form-data')
            .set('Accept', 'application/json');
        setState({ ...state, loading: true });
        this.http.post<User>(environment.baseUrl + environment.signUp, input, config).subscribe(user => {
            if ((<any>user).user_data) {
                this.localStorage.storeOnLocalStorage(environment.getUser, user);
            }
            setState({
                ...state,
                user: <any>user,
                editUser: false,
                loading: false
            });
        });
    }

    @Action(EditUserAction)
    edit({ getState, setState }: StateContext<UserStateModel>, payload: any) {
        const state = getState();
        const input = new FormData();
        const user: User = this.localStorage.loadFromLocalStorage(environment.getUser) as User;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.name ? input.append('name', payload.payload.name) : null;
        input.append('auth_key', user.auth_key);
        input.append('user_id', user.user_data.id);
        // tslint:disable-next-line:no-unused-expression
        payload.payload.email ? input.append('email', payload.payload.email) : null;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.password ? input.append('password', payload.payload.password) : null;
        input.append('device_IMEI', '121332434');
        // tslint:disable-next-line:no-unused-expression
        payload.payload.gender ? input.append('gender', payload.payload.gender) : null;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.birthdate ? input.append('birthdate', payload.payload.birthdate) : null;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.mobile ? input.append('mobile', payload.payload.mobile) : null;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.category_ids ? input.append('category_ids', payload.payload.category_ids) : null;
        // tslint:disable-next-line:no-unused-expression
        payload.payload.area ? input.append('area_id', payload.payload.area) : null;
        setState({ ...state, loading: true });
        if (payload.payload.media) {
            input.append('Media[file]', payload.payload.media);
            this.changeProfilePhoto(input, state, setState);
        } else {
            this.editProfilePhoto(input, state, setState);
        }
    }

    changeProfilePhoto(input: FormData, state: any, setState: any) {
        const config: any = new HttpHeaders().set('Content-Type', 'multipart/form-data')
            .set('Accept', 'application/json');
        this.http.post<User>(environment.baseUrl + environment.changeProfilePhoto, input, config).subscribe(user => {
            this.editProfilePhoto(input, state, setState);
        });
    }

    editProfilePhoto(input: FormData, state: any, setState: any) {
        this.http.post<User>(environment.baseUrl + environment.editUser, input).subscribe(user => {
            if ((<any>user).user_data) {
                this.localStorage.storeOnLocalStorage(environment.getUser, user);
            }
            setState({
                ...state,
                user: <any>user,
                editUser: true,
                loading: false
            });
        });
    }

    @Action(AreasAction)
    areas({ getState, setState }: StateContext<UserStateModel>, payload: any) {
        const state = getState();
        const body = new HttpParams();
        const input: any = new FormData();
        if (payload.payload) {
            body.set('city_id', payload.payload.city_id);
            input.append('city_id', payload.payload.city_id);
        }
        setState({ ...state, loading: true });
        this.http.post<Areas>(environment.baseUrl + environment.getAreas, input).subscribe(areas => {
            setState({
                ...state,
                areas: areas.areas,
                editUser: false,
                loading: false
            });
        });
    }
}