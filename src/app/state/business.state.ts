import { State, Action, StateContext } from '@ngxs/store';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Category } from '../models/Category';
import { Categories } from '../models/Categories';
import { Businesses } from '../models/Businesses';
import { CategoriesAction, SearchBusinessesAction, GetReviewsAction, CreateReviewAction } from '../actions/business.action';
import { environment } from '../../environments/environment';
import { Reviews } from '../models/Reviews';
import { User } from '../models/User';
import { LocalStorageService } from '@local-storage';
import { BaseResponse } from '../models/BaseResponse';

export class BusinessStateModel {
    categories: Category[];
    businesses: Businesses;
    loading: boolean;
    reviews: Reviews;
    error: string;
}
@State<BusinessStateModel>({
    name: 'business',
    defaults: {
        categories: [],
        businesses: null,
        loading: false,
        reviews: null,
        error: ''
    }
})

export class BusinessState {

    constructor(
        private http: HttpClient,
        private localStorage: LocalStorageService) { }

    @Action(CategoriesAction)
    interests({ getState, setState }: StateContext<BusinessStateModel>, payload: any) {
        const state = getState();
        const body = new HttpParams();
        if (payload.payload) {
            body.set('category_id', payload.payload.category_id);
        }
        setState({ ...state, loading: true });
        this.http.post<Categories>(environment.baseUrl + environment.getCategories, body).subscribe(categories => {
            setState({
                ...state,
                categories: categories.categories,
                loading: false
            });
        });
    }

    @Action(SearchBusinessesAction)
    searchBusinesses({ getState, setState }: StateContext<BusinessStateModel>, payload: any) {
        const state = getState();
        const input: any = new FormData();
        setState({
            ...state,
            businesses: null
        })
        if (payload.payload) {
            input.append('name', payload.payload.name);
            input.append('page', payload.payload.page);
            input.append('category_id', payload.payload.category_id ? payload.payload.category_id : null);
        }
        this.http.post<Businesses>(environment.baseUrl + environment.searchBusinesses, input).subscribe(businesses => {
            setState({
                ...state,
                businesses: businesses,
                loading: false
            });
        }, error => {
            setState({
                ...state,
                error: error,
                loading: false
            });
        });
    }

    @Action(GetReviewsAction)
    getReviews({ getState, setState }: StateContext<BusinessStateModel>, payload: any) {
        const state = getState();
        const input: any = new FormData();
        if (payload.payload) {
            input.append('branch_id', payload.payload.branch_id);
        }
        this.http.post<Reviews>(environment.baseUrl + environment.getReviews, input).subscribe(reviews => {
            setState({
                ...state,
                reviews: reviews,
                loading: false
            });
        });
    }

    @Action(CreateReviewAction)
    addReview(payload: any) {
        const input: any = new FormData();
        if (payload.payload) {
            input.append('branch_id', payload.payload.branch_id);
            input.append('text', payload.payload.text);
            const user = (this.localStorage.loadFromLocalStorage(environment.getUser) as User)
            input.append('auth_key', user.auth_key);
            input.append('user_id', user.user_data.id);
            input.append('rating', 3);
        }
        this.http.post<BaseResponse>(environment.baseUrl + environment.addReview, input).subscribe(reviews => {
            console.log(reviews);
        });
    }
}
