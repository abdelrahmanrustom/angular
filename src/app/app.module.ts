import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxCaptchaModule } from 'ngx-captcha';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StorageServiceModule } from 'angular-webstorage-service';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { UserState } from './state/user.state';
import { BusinessState } from './state/business.state';
import { LocalStorageService } from './utils/local.storage';
import { ProfileComponent } from './components/profile/profile.component';
import { HomeComponent } from './components/home/home.component';
import { PreventLoggedInAccess } from './utils/login.access';
import { BusinessesComponent } from './components/businesses/businesses.component';
import { BusinessDetailsComponent } from './components/business-details/business-details.component';
import { MaterialModule } from './material.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
//6LfWQncUAAAAAD19lNKAnrHMmSMTdMVxCEI_JOoG




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent,
    HomeComponent,
    BusinessesComponent,
    BusinessDetailsComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StorageServiceModule,
    MaterialModule,
    NgxCaptchaModule.forRoot({
      reCaptcha2SiteKey: '6LfWQncUAAAAAD19lNKAnrHMmSMTdMVxCEI_JOoG',
      invisibleCaptchaSiteKey: '6LfWQncUAAAAAD19lNKAnrHMmSMTdMVxCEI_JOoG'
      // optional, can be overridden with 'siteKey' component property
    }),
    BrowserAnimationsModule,
    OwlDateTimeModule,
    FlexLayoutModule,
    OwlNativeDateTimeModule,
    NgxPaginationModule,
    NgxsModule.forRoot([UserState, BusinessState]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [LocalStorageService, PreventLoggedInAccess],
  bootstrap: [AppComponent]
})
export class AppModule { }
