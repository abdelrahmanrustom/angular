import { Follow } from './Follow';
import { Area } from './Area';
export interface UserData {
  is_adult_and_smoker: string;
  lang: string;
  birthdate: string;
  name: string;
  id: string;
  profile_photo: string;
  email: string;
  mobile: string;
  no_of_followers: string;
  gender: string;
  lat: string;
  lng: string;
  type: string;
  area: Area;
  no_of_reviews: string;
  no_of_checkin: number;
  no_of_favorites: number;
  categories: Array<string>;
  last_sent_follow: Follow;
  last_received_follow: Follow;
}
