import { BaseResponse } from './BaseResponse';
import { Business } from './Business';
import { Pagination } from './Pagination';

export interface Businesses extends BaseResponse {
    pagination: Pagination;
    businesses: Business[];
}
