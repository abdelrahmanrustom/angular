export interface Category {
  id: number;
  identifier: string;
  name: string;
  description: string;
  main_image: string;
  color: string;
  business_count: number;
  subcategory_count: number;
}
