
export interface Branch {
  id: number;
  name: string;
  address: string;
  operation_hours: string[];
  no_of_reviews: number;
  lng: string;
  lat: string;
  rating: number;
}
