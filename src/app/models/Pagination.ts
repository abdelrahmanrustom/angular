export interface Pagination {
  page_no: number;
  total_pages_no: number;
  total_records_no: number;
}
