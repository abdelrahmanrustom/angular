import { BaseResponse } from './BaseResponse';
import { UserData } from './UserData';

export interface User extends BaseResponse {
  auth_key: string;
  user_data: UserData;
}
