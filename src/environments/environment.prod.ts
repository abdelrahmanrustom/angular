export const environment = {
  production: true,
  baseUrl: 'http://myblabber.com/staging-v2/api/',
  getUser: 'get_user',
  signIn: 'sign-in',
  signUp: 'sign-up',
  editUser: 'edit-profile',
  changeProfilePhoto: 'change-profile-photo',
  getCategories: 'get-categories',
  searchBusinesses: 'search-businesses',
  getReviews: 'get-reviews',
  addReaction: 'add-reaction',
  addComment: 'comment',
  addReview: 'review',
  saveBusiness: 'save_business',
  getAreas: 'get-areas'
};
